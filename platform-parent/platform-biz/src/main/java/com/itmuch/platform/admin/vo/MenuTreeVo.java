package com.itmuch.platform.admin.vo;

import java.util.Date;

import com.itmuch.core.entity.TreeEntity;

public class MenuTreeVo extends TreeEntity<MenuTreeVo> {
    private static final long serialVersionUID = -7111524913053070340L;

    /**
     * 名称.
     */
    private String name;

    /**
     * 排序.
     */
    private Long sort;

    /**
     * 链接.
     */
    private String href;

    /**
     * 目标.
     */
    private String target;

    /**
     * 图标.
     */
    private String icon;

    /**
     * 是否在菜单中显示.
     */
    private String isShow;

    /**
     * 权限标识.
     */
    private String permission;

    /**
     * 创建者.
     */
    private String createBy;

    /**
     * 创建时间.
     */
    private Date createDate;

    /**
     * 更新者.
     */
    private String updateBy;

    /**
     * 更新时间.
     */
    private Date updateDate;

    /**
     * 备注信息.
     */
    private String remarks;

    /**
     * 删除标记.
     */
    private String delFlag;

    private String text;

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSort() {
        return this.sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIsShow() {
        return this.isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getPermission() {
        return this.permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}
